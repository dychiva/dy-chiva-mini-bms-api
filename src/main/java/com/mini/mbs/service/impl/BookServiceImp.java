package com.mini.mbs.service.impl;

import com.mini.mbs.model.Book;
import com.mini.mbs.repository.BookRepository;
import com.mini.mbs.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookServiceImp implements BookService {

    private BookRepository bookRepository;

    @Autowired
    public void setBookRepository(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Override
    public List<Book> findByTitleContainsOrderByTitleAsc(String title) {
        return bookRepository.findByTitleContainsOrderByTitleAsc(title);
    }

    @Override
    public List<Book> findByCategoryTitle(String title) {
        return bookRepository.findByCategoryTitle(title);
    }


}
