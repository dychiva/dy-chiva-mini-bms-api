package com.mini.mbs.service;

import com.mini.mbs.model.Book;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface BookService {

    List<Book> findByTitleContainsOrderByTitleAsc(String title);
    List<Book> findByCategoryTitle(String title);
}
