package com.mini.mbs.controller;


import com.mini.mbs.model.BaseApiResponse;
import com.mini.mbs.model.Book;
import com.mini.mbs.model.Category;
import com.mini.mbs.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/api/v1/categories", produces = { MediaType.APPLICATION_JSON_VALUE })
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class CategoryController {

    @Autowired
    private CategoryRepository categoryRepository;


    @GetMapping("/title/{title}")
    public Category getCategoryName(@PathVariable String title){
         return categoryRepository.findByTitle(title);
    }



//    /**
//     * use for get all record for categories
//     * @return
//     */
//    @GetMapping("")
//    public ResponseEntity getAllCategories(){
//        BaseApiResponse<List<Category>> response = new BaseApiResponse<>();
//        List<Category> categories = categoryRepository.findAll();
//
//        if(categories.isEmpty()){
//            response.setData(categories);
//            response.setStatus(HttpStatus.NOT_FOUND);
//            response.setMessage("Categories failed found");
//            response.setTime(new Timestamp(System.currentTimeMillis()));
//            return ResponseEntity.ok(response);
//        }else {
//            response.setData(categories);
//            response.setStatus(HttpStatus.FOUND);
//            response.setMessage("Categories have been found");
//            response.setTime(new Timestamp(System.currentTimeMillis()));
//            return ResponseEntity.ok(response);
//        }
//    }
//
//
//
//    /**
//     *
//     * @param newCategory for insert new category to Database
//     * @return
//     */
//    @PostMapping("")
//    public ResponseEntity insertCategory(@RequestBody Category newCategory){
//        BaseApiResponse<Category> response =  new BaseApiResponse<>();
//
//        Category category = categoryRepository.save(newCategory);
//        response.setMessage("Category have been inserted");
//        response.setStatus(HttpStatus.OK);
//        response.setTime(new Timestamp(System.currentTimeMillis()));
//        response.setData(category);
//        return ResponseEntity.ok(response);
//    }
//
//
//    @DeleteMapping("/{id}")
//    public ResponseEntity deleteCategory(@PathVariable("id") int id){
//        BaseApiResponse<Category> response = new BaseApiResponse();
////        Optional<Category> category = categoryRepository.findById(id);
////        if(category.isPresent()){
////            response.
////        }
//
//
//        categoryRepository.deleteById(id);
//        response.setMessage("Category has been deleted!");
//        response.setStatus(HttpStatus.OK);
//        response.setTime(new Timestamp(System.currentTimeMillis()));
//        return ResponseEntity.ok(response);
//    }
//
//
//
//    /**
//     * for update the existing record category
//     * @param newCategory , for get the new object category
//     * @param id , to know the which record will be update by pathvariable id
//     * @return
//     */
//    @PutMapping("/{id}")
//    public ResponseEntity<BaseApiResponse<Category>> updateCategory(@RequestBody Category newCategory, @PathVariable("id") int id){
//
//        BaseApiResponse<Category> response = new BaseApiResponse<>();
//        Optional<Category> category = categoryRepository.findById(id);
//
//        if (category.isPresent()) {
//            Category _category = category.get();
//            _category.setTitle(newCategory.getTitle());
//            response.setMessage("Category has been updated!");
//            response.setStatus(HttpStatus.OK);
//            response.setTime(new Timestamp(System.currentTimeMillis()));
//            response.setData(categoryRepository.save(_category));
//            return  ResponseEntity.ok(response);
//        } else {
//            response.setMessage("failed updated!");
//            response.setStatus(HttpStatus.OK);
//            response.setTime(new Timestamp(System.currentTimeMillis()));
//            return ResponseEntity.ok(response);
//        }
//    }

}
