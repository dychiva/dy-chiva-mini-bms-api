package com.mini.mbs.controller;

import ch.qos.logback.core.net.server.Client;
import com.mini.mbs.model.BaseApiResponse;
import com.mini.mbs.model.Book;
import com.mini.mbs.model.Category;
import com.mini.mbs.repository.BookRepository;
import com.mini.mbs.service.impl.BookServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.hateoas.server.mvc.BasicLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.sql.Timestamp;
import java.util.List;

import static org.springframework.hateoas.server.reactive.WebFluxLinkBuilder.linkTo;


//@RepositoryRestController
@RestController
@RequestMapping("/api/v1/books")
@CrossOrigin(origins = "*", allowedHeaders = "*")
@ResponseBody
public class BookController {


    @Autowired
    private BookRepository bookRepository;

    private BookServiceImp bookServiceImp;

    @Autowired
    public void setBookServiceImp(BookServiceImp bookServiceImp) {
        this.bookServiceImp = bookServiceImp;
    }

    @Autowired
    public void setBookRepository(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @GetMapping("/title/{title}")
    public List<Book> searchByTitle(@PathVariable String title){
        return bookServiceImp.findByTitleContainsOrderByTitleAsc(title);
    }

    @GetMapping("/filter")
    public List<Book> searchFilterByCategoryTitle(@RequestParam("category") String categoryTitle){
        return bookServiceImp.findByCategoryTitle(categoryTitle);
    }



//    @PostMapping("")
//    public ResponseEntity insertCategory(@RequestBody Book newBook){
//        BaseApiResponse<Book> response =  new BaseApiResponse<>();
//
//        Book book = bookRepository.save(newBook);
//        response.setMessage("book have been inserted");
//        response.setStatus(HttpStatus.OK);
//        response.setTime(new Timestamp(System.currentTimeMillis()));
//        response.setData(book);
//        return ResponseEntity.ok(response);
//    }
//
//    @GetMapping("")
//    public List<Book> getAllBooks(){
//        return bookRepository.findAll();
//    }
}
