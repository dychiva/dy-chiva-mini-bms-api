package com.mini.mbs.controller;

import com.mini.mbs.service.FilesStorageService;
import org.apache.tomcat.jni.FileInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
public class FilesController {

    @Value(value = "${file.upload.server.path}")
    private String serverPath;

    @Value("${file.base.url}")
    private String imageUrl;

    @Autowired
    FilesStorageService storageService;

    @PostMapping("/upload")
    public ResponseEntity<Map<String,Object>> uploadFile(@RequestParam("file") MultipartFile file) {
        Map<String, Object> res = new HashMap<>();

        String filename = serverPath+file.getOriginalFilename() ;
//        try {
            String fileName = storageService.save(file);
            res.put("message","Uploaded the file successfully");
            res.put("status",true);
            res.put("data",(imageUrl+fileName));
            return ResponseEntity.status(HttpStatus.OK).body(res);
//        } catch (Exception e) {
//
//            res.put("message","Could not upload the file:");
//            res.put("status",false);
//            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(res);
//        }
    }

//    @GetMapping("/files")
//    public ResponseEntity<List<FileInfo>> getListFiles() {
//        List<FileInfo> fileInfos = storageService.loadAll().map(path -> {
//            String filename = path.getFileName().toString();
//            String url = MvcUriComponentsBuilder
//                    .fromMethodName(FilesController.class, "getFile", path.getFileName().toString()).build().toString();
//            System.out.println(url);
//            return new FileInfo(filename, url);
//
//        }).collect(Collectors.toList());
//        System.out.println(fileInfos.listIterator()
//        );
//        return ResponseEntity.status(HttpStatus.OK).body(fileInfos);
//    }

    @GetMapping("/files/{filename:.+}")
    public ResponseEntity<Resource> getFile(@PathVariable String filename) {
        Resource file = storageService.load(filename);
        System.out.println(file);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"").body(file);
    }
}
