package com.mini.mbs.repository;

import com.mini.mbs.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;


@RepositoryRestResource(collectionResourceRel = "books", path = "books")
@CrossOrigin(origins = "*", allowedHeaders = "*")

public interface BookRepository extends JpaRepository<Book, Integer> {

    List<Book> findByTitleContainsOrderByTitleAsc(String title);
    List<Book> findByCategoryTitle(String categoryTitle);

}
