package com.mini.mbs.repository;

import com.mini.mbs.model.Book;
import com.mini.mbs.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;


@RepositoryRestResource(collectionResourceRel = "categories", path = "categories")
@CrossOrigin(origins = "*", allowedHeaders = "*")

public interface CategoryRepository extends JpaRepository<Category, Integer> {

    public Category findByTitle(String title);
}
